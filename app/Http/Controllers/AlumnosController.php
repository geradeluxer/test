<?php

namespace App\Http\Controllers;

use App\Models\Alumnos;
use Illuminate\Http\Request;

class AlumnosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Request
     */
    public function index(Request $request)
    {
        
        // Se debe crear una tabla para los grados
        $grado = array(1 => 'Primero',2 => 'Segundo',3 => 'Tercero',4 => 'Cuarto',5 => 'Quinto',6 => 'Sexto',7 => 'Septimo',8 => 'Octavo',9 => 'Noveno');
        // Se debe crear una tabla para el genero
        $genero = array(1 => 'Masculino',2 => 'Femenino',3 => 'Prefiero no decir',4 => 'Otros');
        
        $alumnos = Alumnos::where('borrado','=',0);

        $filter_grado = $request->input('grado');
        $filter_activo = $request->input('activo');
        //print_r($filter_activo);die;
        if($filter_grado){
            $alumnos = $alumnos->where('grado','=',$filter_grado);
        }else{
            $filter_grado = 0;
        }
        if($filter_activo == '0'){
            $alumnos = $alumnos->where('activo','=',0);
        }elseif($filter_activo == 1){
            $alumnos = $alumnos->where('activo','=',1);
        }else{
            $filter_activo = 1;
        }

        $alumnos = $alumnos->paginate(5);
        
        foreach($alumnos as $key => &$value){
            $value['grado'] = $grado[$value['grado']];
            $value['genero'] = $genero[$value['genero']];
        }

        return view('alumnos.index',compact('alumnos','grado','filter_grado','filter_activo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('alumnos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $alumnos = request()->except('_token');
        $alumnos['activo'] = 1;        
        $alumnos['borrado'] = 0;        

        Alumnos::insert($alumnos);

        return redirect('alumnos');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Alumnos  $alumnos
     * @return \Illuminate\Http\Response
     */
    public function show(Alumnos $alumnos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Alumnos  $alumnos
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Se debe crear una tabla para los grados
        $grado = array(1 => 'Primero',2 => 'Segundo',3 => 'Tercero',4 => 'Cuarto',5 => 'Quinto',6 => 'Sexto',7 => 'Septimo',8 => 'Octavo',9 => 'Noveno');
        // Se debe crear una tabla para el genero
        $genero = array(1 => 'Masculino',2 => 'Femenino',3 => 'Prefiero no decir',4 => 'Otros');
         
        $alumno = Alumnos::findOrFail($id);
        return view('alumnos.edit', compact('alumno','genero','grado'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Alumnos  $alumnos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Se debe crear una tabla para los grados
        $grado = array(1 => 'Primero',2 => 'Segundo',3 => 'Tercero',4 => 'Cuarto',5 => 'Quinto',6 => 'Sexto',7 => 'Septimo',8 => 'Octavo',9 => 'Noveno');
        // Se debe crear una tabla para el genero
        $genero = array(1 => 'Masculino',2 => 'Femenino',3 => 'Prefiero no decir',4 => 'Otros');
                      
        $data = $alumnos = request()->except('_token','_method');
        Alumnos::where('id','=',$id)->update($data);

        $alumno = Alumnos::findOrFail($id);
        return view('alumnos.edit', compact('alumno','grado','genero'));
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Alumnos  $alumnos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Alumnos $alumno)
    {
        //Alumnos::destroy($alumno->id);
        $data['activo'] = 0;
        $data['borrado'] = 0;         
        Alumnos::where('id','=',$alumno->id)->update($data);

        return redirect('alumnos');
    }
}
