@extends('layouts.app')
@section('content')

<div class="container">
    <div class="col-md-6">
    <form action="{{ url('/alumnos') }}" method="post" enctype="multipart/form-data">
        @csrf
        <h1>Editar usuario</h1>
        <div class="form-group">
            <label for="Nombre" class="font-weight-bold">Matricula</label>
            <input type="text" class="form-control" name="matricula" value="">
        </div>
        <div class="form-group">
            <label for="Nombre">Nombre</label>
            <input type="text" class="form-control" name="nombre" value="">
        </div>
        <div class="form-group">
            <label for="ApellidoPaterno">Apellido Paterno</label>
            <input type="text" class="form-control" name="a_paterno" value="">
        </div>
        <div class="form-group">
            <label for="ApellidoMaterno">Apellido Materno</label>
            <input type="text" class="form-control" name="a_materno" value="">
        </div>
        <div class="form-group">
            <label for="FechaNacimiento">Fecha de nacimiento</label>
            <input type="date" class="form-control" name="f_nacimiento" value="">
        </div>
       
        <div class="form-group">        
            <label for="Genero">Genero</label>
            <select class="form-control" name="genero">
                <option value="0">Elige una opcion</option>    
                <option value="1">Hombre</option>    
                <option value="2">Mujer</option>    
                <option value="3">Prefiero no decir</option>    
                <option value="4">otros</option>    
            <select>
        </div>
        <div class="form-group">        
            <label for="Grado">Grado escolar</label>
            <select class="form-control" name="grado">
                <option value="0">Elige una opcion</option>    
                <option value="1">Primero</option>    
                <option value="2">Segundo</option>    
                <option value="3">Tercero</option>    
                <option value="4">Cuarto</option>    
                <option value="5">Quinto</option>    
                <option value="6">Sexto</option>    
                <option value="7">Septimo</option>    
                <option value="8">Octavo</option>    
                <option value="9">Noveno</option>    
            <select>            
        </div>

        <input type="submit" value="Gurdar" class="btn btn-primary">
        
        
        <a href="{{ url('/alumnos') }}" class="btn btn-dark">Regresar</a>

    </form>
    </div>
</div>
@endsection