@extends('layouts.app')
@section('content')

<div class="container">
    <div class="col-md-6">
    <form action="{{ url('/alumnos/'.$alumno->id) }}" method="post">
        @csrf
        {{ method_field('PATCH') }}
        <h1>Editar usuario</h1>
        <div class="form-group">
            <label for="Nombre">Matricula</label>
            <input type="text"  class="form-control" name="matricula" value="{{ $alumno->matricula  }}">
        </div>
        <div class="form-group">
            <label for="Nombre">Nombre</label>
            <input type="text"  class="form-control" name="nombre" value="{{ $alumno->nombre }}">
        </div>
        <div class="form-group">        
            <label for="ApellidoPaterno">Apellido Paterno</label>
            <input type="text"  class="form-control" name="a_paterno" value="{{ $alumno->a_paterno }}">
        </div>
        <div class="form-group">        
            <label for="ApellidoMaterno">Apellido Materno</label>
            <input type="text"  class="form-control" name="a_materno" value="{{ $alumno->a_materno }}">
        </div>
        <div class="form-group">        
            <label for="FechaNacimiento">Fecha de nacimiento</label>
            <input type="date"  class="form-control" name="f_nacimiento" value="{{ $alumno->f_nacimiento }}">
        </div>
        <div class="form-group">        
            <label for="Genero">Genero</label>
            <select class="form-control" name="genero">
                @foreach($genero as $key => $gen)
                    @if($key == $alumno->genero)
                        <option value="{{ $alumno->genero }}" selected>{{ $gen }}</option>
                    @else
                        <option value="{{ $key }}">{{ $gen }}</option>
                    @endif
                @endforeach 
            <select>
        </div>        
        <div class="form-group">        
            <label for="Grado">Grado escolar</label>
            <select class="form-control" name="grado" >
                @foreach($grado as $key => $val)
                    @if($key == $alumno->grado)
                        <option value="{{ $alumno->grado }}" selected>{{ $val }}</option>
                    @else
                        <option value="{{ $key }}">{{ $val }}</option>
                    @endif
                @endforeach
            <select>            
        </div>        
        <div class="form-group">
            <input type="submit" value="Guardar" class="btn btn-primary">
            <a href="{{ url('/alumnos') }}" class="btn btn-dark">Regresar</a>
        </div>

    </form>
    </div>
</div>
@endsection