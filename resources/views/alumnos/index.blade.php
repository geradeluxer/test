@extends('layouts.app')
@section('content')

<div class="container">
    <form action="{{ url('/alumnos') }}" method="get">
    <div class="col-md-12">  
    <div class="row pb-1">  
        <div class="col-md-1">
            <div class="form-group">
                <label for="Grado">Grado escolar</label>
            </div>                
        </div>                
        <div class="col-md-2">
            <div class="form-group">
                <select class="form-control" name="grado" >
                    <option value="0">Todos</option>    
                    @foreach($grado as $key => $val)
                        @if($key == $filter_grado)
                            <option value="{{ $filter_grado }}" selected>{{ $val }}</option>
                        @else
                            <option value="{{ $key }}">{{ $val }}</option>
                        @endif
                    @endforeach
                <select>            
            </div>    
        </div>
        <div class="col-md-1">
            <div class="form-group">
                <label for="Grado">status</label>
            </div>                
        </div>                
        <div class="col-md-2">
            <div class="form-group">
                <select class="form-control" name="activo" >
                            <option value="3" @if($filter_activo == 3) selected @endif >Todos</option>
                            <option value="1" @if($filter_activo == 1) selected @endif >Activo</option>
                            <option value="0" @if(!$filter_activo) selected @endif >Inactivo</option>
                <select>            
            </div>    
        </div>            
        <div class="col-md-2">
            <input type="submit" value="Filtrar" class="btn btn-primary">
        </div>
        <div class="col-md-2 text-right">
            <a href="{{ url('/alumnos/create') }}" class="btn btn-success">Agregar usuario</a>
        </div>        
    </div>
    </div>
    </form>        
        
    <table class="table">
        <thead>
            <tr>
                <th>Matricula</th>
                <th>Nombre</th>
                <th>Apellido Paterno</th>
                <th>Apellido Materno</th>
                <th>Fecha de nacimiento</th>
                <th>Genero</th>
                <th>Grado</th>
            </tr>
        </thead>
        <tbody>
            @forelse($alumnos as $alumno)
            <tr>
                <td>{{ $alumno->matricula }}</td>
                <td>{{ $alumno->nombre }}</td>
                <td>{{ $alumno->a_paterno }}</td>
                <td>{{ $alumno->a_materno }}</td>
                <td>{{ $alumno->f_nacimiento }}</td>
                <td>{{ $alumno->genero }}</td>
                <td>{{ $alumno->grado }}</td>
                <td>
                    <div class="form-group">
                        <a href="{{ url('alumnos/'.$alumno->id.'/edit') }}" class="btn btn-secondary">Editar</a>
                    </div>
                    <form action="{{ url('/alumnos/'.$alumno->id) }}" method="post">
                        @csrf
                        {{ method_field('DELETE') }}
                        <input class="btn btn-danger" type="submit" onclick="return confirm('¿Deseas borrar el registro?')" value="Borrar" >
                    </form>
                </td>
            </tr>
            @empty
            <tr>
                <td>
                    <p>No se encontraron resultados</p>
                </td>
            </tr>
            @endforelse
        <tbody>
    </table>
    @if(count($alumnos) > 0 )
        {{ $alumnos->links('pagination::bootstrap-4') }}
    @endif
</div>

@endsection